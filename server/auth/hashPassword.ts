import crypto from 'crypto';

const AUTH_SECRET_KEY = 'your_super_secret_key';

const hashPassword = (password: string) => {
    const unhashedToken = password + AUTH_SECRET_KEY;

    return crypto.createHash('sha256').update(unhashedToken).digest('hex');
}

export default hashPassword;
