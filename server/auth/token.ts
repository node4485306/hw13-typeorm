const SECRET_KEY = process.env.SECRET_KEY

const token = (password: string) => {
    const salt = (Math.floor(Math.random() * 1000))
    const unhashedToken = password + salt + SECRET_KEY

    const hash = Buffer.from(unhashedToken).toString('base64')

    return hash
}

export default token;
