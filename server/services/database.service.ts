import { Request, Response } from 'express';
import UserRepository from '../typeOrm/userRepository';
import NewspostRepository from '../typeOrm/newspostRepository';

// Клас BaseController є базовим класом для всіх контролерів.
export class DatabaseService {
    

    // Метод для створення нового запису у базі даних для вказаної таблиці.
    async addNews(body: any) {

        const data = await NewspostRepository.create(body);
        
        return data;               
    }

    // Метод для отримання списку записів з бази даних для вказаної таблиці.
    async getList(params: {
        skip: number,
        limit: number
    }) {

        const limit = Number(params.limit) || 2
        const skip = Number(params.skip) || 0

        // Отримання списку записів з бази даних
        const data = await NewspostRepository.readAll(
            {
                limit,
                skip,
            });

        return data;       
    }

    // Метод для отримання одного запису з бази даних
    async getSingle(id: number) {

        const data = await NewspostRepository.read(id);            
            
        return data;       
    }

    // Метод для оновлення одного запису
    async updateNews(id: number, body: any, res: Response) {
        const record = await NewspostRepository.read(id);

        if (!record) {
            return res.status(404).json({ message: 'Post not found' });
        }

        const data = await NewspostRepository.update(id, body);
        return data;
    }

    // Метод для видалення одного запису
    async deleteNews(id: number, res: Response) {
        const record = await NewspostRepository.read(id);

        if (!record) {
            return res.status(404).json({ message: 'Post not found' });
        }

        const data = await NewspostRepository.delete(id);
        return data;
    }

}

