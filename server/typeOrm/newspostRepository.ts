import { AppDataSource } from './data-source'; 
import { Newspost } from './entity/Newspost';
import { User } from './entity/User';

class NewspostRepository {
    private static instance: NewspostRepository;
    private db: any;

    constructor() {
        this.db = AppDataSource.getRepository(Newspost);
    }

    public static getInstance(): NewspostRepository {
        if (!NewspostRepository.instance) {
        NewspostRepository.instance = new NewspostRepository();
        }
        return NewspostRepository.instance;
    }

    async create(data: any) {
        try {
            const newspost = new Newspost();
            newspost.title = data.title;
            newspost.content = data.content;
            newspost.genre = data.genre;
            // newspost.author = data.user.id;
            console.log(data);

            return this.db.manager.save(newspost);

        } catch (error) {
            console.error('Error creating newspost:', error);
            throw error;
        }
        
    }

    async readAll(params: { skip: number, limit: number }) {
        try {
            const items = await this.db.find({
                skip: params.skip,
                take: params.limit,
                relations: ["author"]
            });
            const count = await this.db.count();
            return { items, count };

        } catch (error) {
            console.error('Error reading newsposts:', error);
            throw error;
        }
    }

    async read(id: number) {
        try {
            return this.db.findOne({ relations: ["author"], where: { id } });
            
        } catch (error) {
            console.error('Error reading newspost:', error);
            throw error;
        }
        
    }

    async update(id: number, newData: any) {
        try {
            
            return this.db.update({ where: { id } }, newData);
            
        } catch (error) {
            console.error('Error updating newspost:', error);
            throw error;
        }
    }

    async delete(id: number) {
        try {
            await this.db.delete({ where: { id } });
            return {message: `Newspost with id ${id} deleted`};
        } catch (error) {
            console.error('Error deleting newspost:', error);
            throw error;
        }
    }
}

export default NewspostRepository.getInstance();
