import { AppDataSource } from './data-source'; 
import { User } from './entity/User';

class UserRepository {
    private static instance: UserRepository;
    public db: any;

    constructor() {
        this.db = AppDataSource.getRepository(User);
    }

    public static getInstance(): UserRepository {
        if (!UserRepository.instance) {
        UserRepository.instance = new UserRepository();
        }
        return UserRepository.instance;
    }

    async createUser(data: any) {
        try {
            const user = new User();
            user.email = data.email;
            user.password = data.password;

            return this.db.manager.save(user);

        } catch (error) {
            console.error('Error creating user:', error);
            throw error;
        }
        
    }


    async read(email: string) {
        try {
            return this.db.findOne({ where: { email } });
            
        } catch (error) {
            console.error('Error reading user:', error);
            throw error;
        }
        
    }

}

export default UserRepository.getInstance();
