import { BaseController } from './base.controller';


// клас PostController є нащадком класу BaseController
export class PostController extends BaseController  {
  // конструктор класу PostController
  constructor () {
    // виклик конструктора базового класу де вказано назву таблиці з якою буде працювати контролер
    super('posts');
  }

}