import { BaseController } from './base.controller';

// клас UserController є нащадком класу BaseController
export class UserController extends BaseController {
  constructor() {
    // виклик конструктора базового класу де вказано назву таблиці з якою буде працювати контролер
    super('users');
  }

  
}
