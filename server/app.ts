import express, { Application, NextFunction, Request, Response } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { MainController, UserController, PostController, AuthController } from './controllers'
import Routes from './routes'
// import ViewRouter from './routes/view.routes'
import { DatabaseService } from './services'
import dotenv from 'dotenv'
import cors from 'cors'
import createPostMiddlewares from './validation/validation'
import { logRequests, logError } from './logger/logger'
import { AppDataSource } from './typeOrm/data-source'


const PORT = process.env.PORT || 8000

// cors опції, вказуємо, які ресурси мають доступ до сервера
const CORS_OPTIONS = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}

class App {
    private app: Application
    private mainController: MainController
    private userController: UserController
    private postController: PostController
    private authController: AuthController



    constructor() {
        // ініціалізація express
        this.app = express()

        this.mainController = new MainController()
        this.userController = new UserController()
        this.postController = new PostController()
        this.authController = new AuthController()

    }

    configureDbConnection() {
        if (!AppDataSource.isInitialized) {
        AppDataSource.initialize()
            .then(() => {
                console.log("Data Source has been initialized!")
            })
            .catch((err) => {
                console.error("Error during Data Source initialization", err)
            })
        }
    }


    // маршрутизація
    routing() {
        this.app.get('/', this.mainController.getStartPage)

        this.app.get('/api/user', this.authController.getUser)
               
        this.app.post('/api/post', createPostMiddlewares, this.postController.addNews)

        this.app.post('/api/login', this.authController.signIn) // за допомогою цього роута ми логинемось
        this.app.post('/api/register', this.authController.registration) // за допомогою цього роута ми реєструємось


        // маршрутизація для API
        Object.keys(Routes).forEach((key) => {
            this.app.use(`/api/${key}`, Routes[key])
        })
    }

    // ініціалізація плагінів які працюються як міддлвари
    initPlugins() {
        this.app.use(bodyParser.json())
        this.app.use(morgan('dev'))
        this.app.use(cors(CORS_OPTIONS))
        this.app.use(logRequests);
        this.app.use(logError);
        
    }

    // запуск сервера
    async start() {
        // if (process.env.NODE_ENV !== 'production') {
        //     //await DatabaseService.dropTables()
        //     await DatabaseService.createTables()
        // }

        this.configureDbConnection()
        this.initPlugins()
        this.routing()

        this.app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`)
        })
    }
}

// ініціалізація dotenv
dotenv.config()

// створення нашого сервера
const app = new App()

// запуск сервера
app.start()


