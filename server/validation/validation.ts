import { NextFunction, Request, Response } from 'express'
import Validator from 'fastest-validator'

const validatorService = new Validator();

const validatorMiddleware = (schema: any) => {
    const checkService = validatorService.compile(schema);

    return (req: Request, res: Response, next: NextFunction) => {
        const check: any = checkService(req.body);
        if (check.length > 0) {
            res.status(400);
            res.json({ message: 'Validation error', errors: check });
            return;
        }

        next();
    };
}

const createPostMiddlewares = [
            validatorMiddleware({
                title: { type: "string", min: 3, max: 50 },
                content: { type: "string", min: 3, max: 255 },
                genre: { type: "string", enum: ['Politic', 'Business', 'Sport', 'Other'] },
                createDate: { type: "string" },
                isPrivate: { type: "boolean" }
            })
        ]


export default createPostMiddlewares;
