export default {
    name: 'users',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },       
        {
            name: 'password',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        }
        
    ]
}