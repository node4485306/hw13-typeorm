export default {
    name: 'posts',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },
        {
            name: 'title',
            type: 'string'
        },
        {
            name: 'content',
            type: 'string'
        },
        {
            name: 'genre',
            type: 'string'
        },       
        {
            name: 'createDate',
            type: 'string'
        },
        {
            name: 'isPrivate',
            type: 'string'
        },
        {
            name: 'userId',
            type: 'integer'
        }

    ]
}