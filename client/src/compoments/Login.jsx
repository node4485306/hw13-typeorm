import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Login() {
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch('http://localhost:8000/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password })
            });

            if (response.ok) {
                console.log('Login successful');
                const data = await response.json();
                const token = data.token.replace('Bearer ', ''); 
                localStorage.setItem('TOKEN', token);
                window.location.href = '/';
            } else {
                console.error('Login failed');
            }

        } catch (error) {
            console.error('Error logging in:', error);
        }
    };


    return (  
        <div className="container">
            <h2>Авторизація</h2>
            <form onSubmit={handleSubmit}>
                <div className='input-wrapper'>
                    <label>Email:</label>
                    <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <button type="submit" className='btn'>Увійти</button>
            </form>
            <p>Немає аккаунта? <Link to="/registration"> Зареєструйтесь</Link>
            </p>
        </div>
    );
}

export default Login;